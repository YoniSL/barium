package barium_server;

import java.net.Socket;

public abstract class CommandHandler {
	
	public static void nick(String name, ClientHandler ch){
		ch.setName(name);
		ch.sendToClient("Succesfully changed name to " + name + ".");
	}
	
	public static void ban(String ip, ClientHandler ch){
		ClientHandler client = ConnectionHandler.findClient(ip);
		
		if(kick(client)){
			ConnectionHandler.addToBanList(ip);
			alertToAll(client.getName() + " is banned from the server.");
		}
		else
			ch.sendToClient("Could not ban " + ch.getName() + ".");
	}
	
	public static void unban(String ip, ClientHandler ch){
		if(ConnectionHandler.findClient(ip) != null){
			ConnectionHandler.removeFromBanList(ip);
			alertToAll(ip + " is unbanned from the server.");
		}
		else 
			ch.sendToClient(ip + " was never banned.");
	}
	
	public static void kick(String ip, ClientHandler ch){
		ClientHandler client = ConnectionHandler.findClient(ip);
		
		if(kick(client))
			alertToAll(client.getName() + " got kicked from the server.");
		else
			ch.sendToClient("Could not kick " + client.getName() + ".");
	}
	
<<<<<<< HEAD
	private static Boolean kick(ClientHandler client){
		if(client != null) {		
			Socket s = client.getSocket();
			
			try {
				s.close();
				ConnectionHandler.removeFromClients(client);
				return true;
			}
			catch(Exception e) {
=======
	private static Boolean kick(ClientHandler client) {
		if(client != null) {		
			try {
				client.sendToClient("You have been kicked");
				client.stop();
				ConnectionHandler.removeFromClients(client);
				return true;
			} catch(Exception e) {
>>>>>>> 5bd37efb2d6f754f1855a517235faec40b2f6316
				Main.log(e.getMessage());
			}
		}
		
		return false;
	}

	// Message to all clients without own name
	public static void alertToAll(String message) {
		ConnectionHandler.sendMessageToAll(message);
	}
	
	
	//Message to all clients with name
	public static void sendToAll(String message, ClientHandler ch) {
		alertToAll(ch.getName() + " : " + message);
	}
	
	public static void invalidCommand(ClientHandler ch) {
		Main.log("Wrong command received from " + ch.getName());
	}
}
