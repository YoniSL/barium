package 	barium_server;

import java.io.*; 

public class Main {	
	private static ConnectionHandler ch;
	private static boolean stop = false;
	private static BufferedReader br;
	
	public static void main(String[] args) { 
		String buffer;	
		br = new BufferedReader(new InputStreamReader(System.in));
		
		System.out.println("Please enter a port above 1000:");
		ch = new ConnectionHandler(askForPort()); // ConnectionHandler accepts the clients
	
		try {
			System.out.println("Server started listening on port " + ch.getPort());
			while(!stop && (buffer = br.readLine().toLowerCase()) != null) { // Local command line commands (console)
				switch(buffer) {
					case "stop":
					case "exit":
						ch.stop();
						stop = true;
						break;
					case "help":
						Main.log("Available commands : stop/exit, list, help");
						break;
					case "clientlist":
						for(ClientHandler c : ch.getClients()) {
							Main.log(c.getName() + " is " + (c.clientSocket.isConnected() ? "connected" : "ded"));
						}
						break;
					case "banlist":
						for(String bannedIp : ch.getBannedIps())
							Main.log(bannedIp);
						break;
					default:
						log("Unknown command.");
						break;
				}
			}
		} catch(Exception e) {
			// e.printStackTrace();
		}
	}

	private static int askForPort() {
		int port = 0;
		while(port < 1000) {
			try {
				port = Integer.parseInt(br.readLine());
				if(port < 1000)
					log("Port is below 1000. Try again:");
			} catch(Exception e){
				log("Port is not a number. Try again:");
			}
		}
		return port;
	}
	
	public static void log(String msg) {
		System.out.println(msg);
	}
	
	public static boolean stopped() {
		return stop;
	}
}

