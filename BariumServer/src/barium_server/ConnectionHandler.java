package barium_server;

import java.util.*; // List
import java.io.IOException;
import java.net.*;

public class ConnectionHandler implements Runnable {
	private int port;
	private ServerSocket server;
	private int amountOfClients = 0;
	
	private static ArrayList<String> banned = new ArrayList<String>();
	private static ArrayList<ClientHandler> clients = new ArrayList<ClientHandler>();
	
	ConnectionHandler() {
		this(4444);
	}
	
	ConnectionHandler(int port) {
		this.port = port;  
		Thread t  = new Thread(this);
        t.start();
	}
	
	public int getPort() {
		return this.port;
	}
	
	public ArrayList<ClientHandler> getClients() {
		return clients;
	}
	
	public ArrayList<String> getBannedIps() {
		return banned;
	}
	
	public void run() {
		try {
			server = new ServerSocket(port);
		
			while(!Main.stopped()) {
				Socket s = server.accept();
				String ip = s.getInetAddress().toString().replaceAll("/", "").trim();
				
				if(!banned.contains(ip)) {	
					ClientHandler ch = new ClientHandler(s);
					amountOfClients++;
					
					 // set unique default name
					ch.setName("Anon[" + amountOfClients + "]");
					
					clients.add(ch);
				} else 
					s.close();
			}
		} catch(Exception e) {
			Main.log("Server listening ended");
		}

		exit();
	}
	
	public static ClientHandler findClient(String ip){
	 	for(ClientHandler ch : clients){
	 		if(ch.getIp().equals(ip)){
	 			return ch;
	 		}
	 	}
	 	
	 	return null;
	}
	
	public static void addToBanList(String ip){
		banned.add(ip);
	}
	
	public static void removeFromBanList(String ip){
		banned.remove(ip);
	}
	
	public static void removeFromClients(ClientHandler client) {
		int i = 0;
		for(ClientHandler c : clients) {
			if(c.getName() == client.getName())
				clients.remove(i);
			
			i++;
		}
		
		// clients.remove(client);
	}
	
	public static void sendMessageToAll(String message) {
		for(ClientHandler ch : clients) {
			if(ch.clientSocket.isConnected()) {
				try {
				
					byte msgBytes[] = new byte[message.length()];
					int i = 0;
					for(char a : message.toCharArray()) {
						msgBytes[i] = (byte)a;
						i++;
					}
					
					ch.clientSocket.getOutputStream().flush();
					ch.clientSocket.getOutputStream().write(msgBytes);
				} catch(Exception e) {
					System.out.println(e.getMessage());
				}
			} else {
				removeFromClients(ch);
			}
		}
	}
	
	public void stop() {
		exit();
	}
	
	private void exit() {
		try {
			server.close();
			for(ClientHandler c : clients)
				c.stop();
		} catch(IOException e) {
			// e.printStackTrace();
		}
	}
}