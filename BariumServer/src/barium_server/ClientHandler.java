package barium_server;

import java.io.*;
import java.net.*;
import java.util.Arrays;

public class ClientHandler implements Runnable {
	public Socket clientSocket;
	private String name;
	private String ip;
	private boolean stopped;
	
	ClientHandler(Socket clientSocket) {
		this.clientSocket	= clientSocket;
		this.ip 			= clientSocket.getInetAddress().toString().replaceAll("/", "");
		this.stopped		= false;
		Thread t			= new Thread(this);
		t.start();
	}
	
	public void run() {
		try {			
			while(!Main.stopped() && !this.stopped) {
				String bufferString = "";
				String parameters[];
				byte buffer[] = new byte[1024];

				clientSocket.getInputStream().read(buffer);

				for(byte b : buffer) {
					if(b != 0)
						bufferString += (char)b;
					else
						break;
				}
				
				if(bufferString.length() == 0) {
					this.stopped = true;
					ConnectionHandler.removeFromClients(this);
				}
				
				// Get all parameters from buffer
				parameters = Arrays.copyOfRange((parameters = bufferString.split(":")), 1, parameters.length);
				
				//first term in buffer is the command
				runCommand(bufferString.split(":")[0], parameters);
			}	
			
			disconnect();
		} catch(Exception e) {
			ConnectionHandler.removeFromClients(this);
			// e.printStackTrace();
		}
	}

	public Socket getSocket() {
		return clientSocket;
	}
	
	public String getName() {
		return name;
	}
	
	public String getIp() {
		return ip;
	}
	
	public void setName(String name) {
		this.name = name;
	}
	
	public void sendToClient(String message) {
		if(clientSocket.isConnected()) {
			try {
				byte msgBytes[] = new byte[message.length()];
				int i = 0;
				
<<<<<<< HEAD
				for(byte b : message.getBytes()) {
					msgBytes[i] = b;
					i++;
				}
					
				clientSocket.getOutputStream().flush();
				clientSocket.getOutputStream().write(msgBytes);
			} catch(Exception e) {
				Main.log("Failed to send message to client, did the client dc?");
			}
		} else {
			ConnectionHandler.removeFromClients(this);
=======
			clientSocket.getOutputStream().flush();
			clientSocket.getOutputStream().write(msgBytes);
		} catch(Exception e) {
			ConnectionHandler.removeFromClients(this);
			Main.log("Failed to send message to client - client removed");
>>>>>>> 5bd37efb2d6f754f1855a517235faec40b2f6316
		}
	}
	
	private void runCommand(String com, String[] parameters){
		
		/* 
		 * All the commands that are run on the server only need one parameter.
		 * Since that might change in the future, leave parameters[] alone =(
		 * For now, the parameters are added together in parameterString
		 * and passed to CommandHandler.method()
		 */
		if(com == "" || com.length() == 0)
			return;
		
		String parameterString = "";
		
		//Get all terms after the command
		for(String c: parameters)
			if(c != null)
				parameterString += c;
		
		switch(com) {
			case "nick":
				CommandHandler.nick(parameterString, this);
				break;
			case "ban":
				CommandHandler.ban(parameterString, this);
				break;
			case "unban":
				CommandHandler.unban(parameterString, this);
				break;
			case "kick":
				CommandHandler.kick(parameterString, this);
				break;
			case "msg":
				CommandHandler.sendToAll(parameterString, this);
				break;
			case "disc":
				ConnectionHandler.removeFromClients(this);
				Main.log("Client disconnected");
				this.stopped = true;
				break;
			default:
				CommandHandler.invalidCommand(this);
				break;
		}
	}
	
	private void disconnect() throws IOException {
		String message = this.name + " disconnected";
		clientSocket.close();
		ConnectionHandler.removeFromClients(this);
		CommandHandler.alertToAll(message);
	}
	
	public void stop() {
		try {
			clientSocket.close();
		} catch(Exception e) {
			
		}
	}
}