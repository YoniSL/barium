package barium_client;

public class CommandHandler {
	private ConnectionHandler ch;
	
	public CommandHandler(ConnectionHandler ch){
		this.ch = ch;
	}
	
	public Boolean connect(String[] parameters) {
		//4444 is the default port
		int port = 4444;
		
		if(parameters.length != 1 && parameters.length != 2)
			return false;
		
		if(parameters.length == 2) {
			try{
				port = Integer.parseInt(parameters[1]);
			} catch(Exception e){
				return false;
			}
		}
		
		//start connection
		ch = new ConnectionHandler(parameters[0], port);
		
		return true;
	}
	
	public void kick(String[] parameters) {
		if(ch != null) {
			if(parameters.length == 1)
				ch.sendCommand(Command.kick, parameters[0]);
			else
				Main.log("wrong parameter, use it like /kick [username]");	
		} else 
			Main.log("Not connected.");
	}
	
	public void ban(String[] parameters) {
		if(ch != null) {
			if(parameters.length == 1)
				ch.sendCommand(Command.ban, parameters[0]);
			else
				Main.log("wrong parameter, use it like /ban [ip]");
		}
		else 
			Main.log("Not connected.");
	}
	
	public void unban(String[] parameters) {
		if(ch != null) {
			if(parameters.length == 1)
				ch.sendCommand(Command.unban, parameters[0]);
			else
				Main.log("wrong parameter, use it like /unban [ip]");
		} else
			Main.log("Not connected.");
	}
	
	public void disconnect() {	
		ch.close();
	}
	
	public void nick(String[] parameters) {
		if(ch != null) {
			if(parameters.length == 1)
				ch.sendCommand(Command.nick, parameters[0]);
			else
				Main.log("wrong parameter, use it like /nick [username]");
		} else
			Main.log("Not connected.");
	}
	
	public void msg(String message) {
		if(ch != null) {
			ch.sendCommand(Command.msg, message);
		} else
			Main.log("Not connected.");
	}
}
