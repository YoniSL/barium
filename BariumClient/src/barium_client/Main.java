package barium_client;

import java.io.*;
import java.util.Arrays;

public class Main {
	private static boolean stop		= false;
	
	public static void main(String[] args) throws IOException {
		String buffer;
		String parameters[];
		String commands[]			= {
										"connect [ip:port]", "help/commands", "nick/nickname [name]",   
										"kick [username]", "ban [ip]", "unban [ip]", 
										"stop/exit"
									};	
		
		BufferedReader br 			= new BufferedReader(new InputStreamReader(System.in));
		ConnectionHandler ch		= null;
		CommandHandler commHandler	= new CommandHandler(ch);
		
		try {			
			while(!stop && (buffer = br.readLine().toLowerCase()) != null) {
				
				// Get all parameters from buffer
				parameters = Arrays.copyOfRange((parameters = buffer.split(" ")), 1, parameters.length);
				
				//first term in buffer is the command
				String com = buffer.split(" ")[0];
				
				switch(com) {
					case "connect":
						if(!commHandler.connect(parameters))
							Main.log("wrong parameter(s), use it like connect [ip] [port]");
						break;	
					case "disc":
					case "disconnect":
						commHandler.disconnect();
						break;
					case "nick":
					case "nickname":
						commHandler.nick(parameters);
						break;
					
					case "kick":
						commHandler.kick(parameters);
						break;
						
					case "ban":
						commHandler.ban(parameters);						
						break;
						
					case "unban":
						commHandler.unban(parameters);
						break;
						
					case "exit":
					case "stop":
						commHandler.disconnect();
						stop = true;
						break;
						
					case "help":
					case "commands":
						for(String c: commands)
							System.out.println(c);
						break;
					default:
						commHandler.msg(buffer);
						break;
				}
			}
		} catch(Exception e) {
			log("Error: ' " + e.getMessage() + " '");
		} finally {
			if(ch != null)
				ch.close();
			
			log("Closing program.");
		}
	}
	
	public static void log(String msg) {
		System.out.println(msg);
	}
}
