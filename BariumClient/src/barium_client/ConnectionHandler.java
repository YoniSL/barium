package barium_client;

import java.net.*;
import java.io.*;

public class ConnectionHandler implements Runnable {
	private String ip;
	private int port;
	private Socket connectionSocket;
	private boolean stopped;
	public Thread t;
	
	public ConnectionHandler(String ip) {
		this(ip, 4444);
	}
	
	public ConnectionHandler(String ip, int port) {
		this.ip = ip;
		this.port = port;
		this.stopped = false;
		t = new Thread(this);
        t.start();
	}
	
	public void run() { // Reader, waits for packets from server
		try {
			connectionSocket = new Socket(ip, port);
			
			while(!this.stopped) {
				byte buffer[]	= new byte[1024]; // 1KB buffer, this will suffice for 1024 chars/bytes
				String reply	= "";
				connectionSocket.getInputStream().read(buffer);
				
				for(byte b : buffer) {
					if(b != 0)
						reply += (char)b;
					else
						break;
				}
				
				if(reply.length() == 0)
					close();
				else
					Main.log(reply);
			}
		} catch(Exception e) {
			Main.log(e.getMessage());
		}
	}
	
	public void sendCommand(Command command, String[] parameters) {
		String commandString = command.name();
		
		for(String c: parameters)
			commandString += ":" + c;
		
		try {
			byte msgBytes[] = new byte[commandString.length()];
			int i = 0;
			
			for(byte b : commandString.getBytes()) {
				msgBytes[i] = b;
				i++;
			}
			
			connectionSocket.getOutputStream().flush();
			connectionSocket.getOutputStream().write(msgBytes);
		} catch (Exception e) {
			Main.log("Could not perform action.");
		}
		
	}
	
	public void sendCommand(Command command, String value) {
		this.sendCommand(command, new String[]{ value });
	}
	
	public void close(){
		try {
			connectionSocket.close();
			this.stopped = true;
			Main.log("Disconnected");
		} catch (IOException e) {
			// e.printStackTrace();
		}
	}
}
